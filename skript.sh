#!/bin/bash


if [ $EUID -ne 0 ] 
then
    echo "Pole root!" 
    exit 1;  
fi
#Kontrolllause, et veenduda root õigustes

if [ $# -eq 2 ]; then
 	KAUST=$1
    	GROUP=$2
    	SHARE=$(basename $KAUST)
    else
        if [ $# -eq 3 ]; then
            KAUST=$1
            GROUP=$2  
            SHARE=$3
            else
                echo "Usage $(basename $0) KAUST GRUPP [SHARE]"
                exit 1;
            fi
fi
#Vajalike sisendite kontroll


if [ ${KAUST:0:1} == "/" ];  then
	KAUST=$KAUST
else
	KAUST=$(pwd)/$KAUST
fi
#kontroll, kas sisse on antud täispikk tee või mitte   


type smbd > /dev/null 2>&1

if [ $? -ne 0 ]; then
    apt-get update && apt-get install samba -y
else
    echo 'Samba on olemas'
fi
#Kontrollib, kas samba on installeeritud

#Testin kausta olemasolu test(Directory) käsuga, vajadusel loon kausta.
mkdir -p $KAUST


getent group $GROUP > /dev/null || addgroup $GROUP > /dev/null
#Kontrollin, kas grupp on olemas, kui mitte, siis loon.


#Backup
cp /etc/samba/smb.conf /etc/samba/smb.conf.backup

#Tühi rida viimaseks
cat >> /etc/samba/smb.conf.backup << EOF

[$SHARE]
    comment=share folder
    path=$KAUST
    writable=yes
    valid users=@$GROUP
    force group=$GROUP
    browsable=yes
    create mask=0664
    directory mask=0775
EOF

#Kontrollin testi confi(Kas ridade lisamine õnnestus)
testparm -s /etc/samba/smb.conf.backup
if [ $? -ne 0 ]; then
    echo "VIGA!!!"
    exit 1;
fi

#Kopeerin õige confi üle
cp /etc/samba/smb.conf.backup /etc/samba/smb.conf

#Samba servicei restart
/etc/init.d/smbd reload


echo "Skripti töö on lõppenud"

