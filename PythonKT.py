#!/usr/bin/python
# -*- coding: utf-8 -*-


import random, string, sys, os.path, itertools

#Argumentide kontroll
if len(sys.argv) != 3:	
	print("Sisesta argumendid - sisendfail ja v2ljundfail")
	exit(0)
elif os.path.exists(sys.argv[1]) == 0:
	print("Antud sisendfail puudub")
	exit(0)

#Avan sisendfaili
#fileObject = open(sys.argv[1])
L = []
#Loen sisse sisendfailist info
with open(sys.argv[1]) as f:
	for line in itertools.islice(f, 0, None, 2):
		L.append(line.strip("\n").split("\t"))


#L.pop(0)
kustutada = [0, 0]
for i in sorted(kustutada, reverse=True):
	del L[i]
L.pop()

#Random funktsioon tokeni jaoks
def rand():
    x=""
    while len(x) < 18:
        x += x.join(random.choice(string.letters))
    return(x)

end = []

#Avan/loon salvestusfaili
salvesta = open(sys.argv[2], 'w')

#Vajalike ridade kirjutamine faili
for line in L:
    if line != [""]:
        end.append(line[1].split())
for element in end:
	print >> salvesta, element[0][0:1]+element[1][0:7]+",",element[0]+" "+element[1]+",",element[0][0:1]+element[1][0:7]+"@itcollege.ee,",element[2]+rand()