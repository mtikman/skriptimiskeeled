#!/bin/bash


export LC_ALL=C
export LANG=C

#Juurikas või mitte?
if [  $UID -ne 0 ]
then
   echo "$(basename $0) tuleb k2ivitada rootis!"
   exit 1   
fi

#Muutujate arv õige?
if [ $# -eq 1 ]; then
   NAME=$1

   else
      echo "Kasuta skripti $(basename $0) VEEBIAADRESS"
      exit 1
fi   

#Apache olemas? Kui mitte, paigaldab.
type apache2 > /dev/null 2>&1

if [ $? -ne 0 ]
then
   apt-get update && apt-get install apache2 -y || exit 1
fi

#Nimelahenduse lisamine
echo "127.0.0.1  $NAME" >> /etc/hosts

#confi kopeerimine
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$NAME.conf

#Confi muutmine
sed -i "s-#ServerName www.example.com-ServerName $NAME-g" /etc/apache2/sites-available/$NAME.conf
sed -i "s-/var/www/html-/var/www/$NAME-g" /etc/apache2/sites-available/$NAME.conf


#Kodukataloogi loomine
mkdir -p /var/www/$NAME


#Apache indexfaili kopeerimine
cp /var/www/html/index.html /var/www/$NAME/index.html


#Indexfaili modimine
echo "<html><head><title></title></head><body><pre>
                                                                                        
                                                                                        
                                      Thepathof                                         
                                  therighteousmanis                                     
                              besetonallsidesbytheiniq                                  
                      uitiesoftheself           ishandth                                
                   etyrannyofevi                  lmen.Bl                               
                 essedishewho,in                   thenam                               
                 eofcharityandgoo                   dwill                               
                 ,shepherdst heweak    throughthev  alley                               
                 ofdarkness,forheist rulyhisbrother' skee                               
                 perandthefinderof  lostchildren.AndIwill                               
                strik  edownuponth  eewithgreatvengeancea                               
               ndfuriousangerthosew howouldatte mpttopois                               
              onanddestroyMybroth   ers.AndyouwillknowMyn                               
             ameistheLordwhenIlayMyvengeanceupont  hee.T                                
            hepat          hoftherighteousman     isbese                                
           tonal                      lsidesb     ythein                                
          iquiti                                 esofth                                 
         eselfi                                 shandt                                  
        hetyra                                  nnyofe                                  
        vilme                      n.Bl        essedi                                   
        shew                      ho,in the   nameof                                    
        char                      ityandgood  will,                         shepherds   
       thewe                      akthrough  theva                        lleyofdarkne  
       ss,fo                     rheistruly hisbr                       other'    skee  
       peran                     dthefinde  roflo                     stchild    ren.A  
       ndIwi                    llstriked  ownupo                   ntheewi     thgre   
       atven                    geancean   dfuriousangerthosewh   owoulda     ttemp     
        ttop                   oisonand    destroyMybrothers.Andyouwill      knowM      
        ynam                   eistheL     ordwh   enIla   yMyvengean      ceupon       
        thee                  .Thepath      oft   herighteousmanisb      eseton         
        allsi               desby thein         iquitiesoftheselfi     shandth          
         etyr             annyo  fevilme         n.Blessedishewho,in   thenameo         
         fchar            ityandgoodwill                     ,shepher    dstheweak      
          throu            ghthevalleyo              fdar       kness,  forh eistr      
          ulyhis              brot                   her'        skeepe  randthef       
           indero                                flo              stchi    ldre         
            n.AndIwi                            llst              riked     ownu        
               ponthee                          with              greatvengeance        
     and        furiousang                       erth           osewhowouldatte         
    mpttopo    isonanddestroyMyb                  rot         hers.An    d              
    youwillknowMyn ameistheLordwhenIlayM           yven    geanceu                      
    pont hee.Thepathoft    herighteousmani sbesetonallsidesbythe                        
     iniq  uitiesofth         eselfishand thetyrannyofevilmen                           
      .Ble   ssedis         hewho,inthen ameof charityandg                              
       oodwill,sh           epherdsthew  eakt                                           
        hrought              hevalley   ofda                                            
          rkn                ess,fo    rhei                                             
                              struly  hisb                                              
                               rother'ske                                               
                                 eperand                                                
                                   the                                                  

</pre></body></html>" > /var/www/$NAME/index.html


#Virtuaalserveri enablemine
a2ensite $NAME > /dev/null 2>&1

#Apache restart
/etc/init.d/apache2 reload > /dev/null 2>&1
exit 0
