#!/bin/bash


if [ $EUID -ne 0 ] 
then
    echo "Pole root!" 
    exit 1;  
fi

if [ $# -eq 1 ]; then
        PIN=$1
        else
            printf "%.4d " {0..9999} > PIN.txt
            exit 0;
        fi

for i in $(seq -f %04g 0 9999)
do
echo $i
if [ $i -lt $PIN ]; then
    echo $i 
else
    if [ $i -eq $PIN ]; then
    mkdir -p $PIN     
else
    if [ $i -gt $PIN ]; then
    echo $i >> $PIN/koodid.txt
fi
fi
fi
done
